const ap = new APlayer({
    container: document.getElementById('player'),
    mini: true,
    autoplay: true,
    theme: '#FADFA3',
    loop: 'all',
    order: 'random',
    preload: 'auto',
    volume: 0.7,
    mutex: true,
    listFolded: false,
    listMaxHeight: 90,
    lrcType: 3,
    audio: [
        {
            name: '砂时针',
            artist: '夢乃ゆき',
            url: 'https://music.163.com/song/media/outer/url?id=440101619.mp3',
            cover: 'http://p2.music.126.net/GilgWIa6pK5KLYCj4XBA7w==/3299634401479878.jpg',
            theme: '#46718b'
        },
        {
            name: '刹那の果実',
            artist: '黒崎真音',
            url: 'https://music.163.com/song/media/outer/url?id=32069273.mp3',
            cover: 'http://p2.music.126.net/REqSktH6PETWbeAEXrMGlg==/109951163534526538.jpg',
            theme: '#46718b'
        },
      {
            name: 'A/Z',
            artist: '泽野弘之',
            url: 'https://music.163.com/song/media/outer/url?id=29027056.mp3',
            cover: 'http://p2.music.126.net/FahHKWzpgA5t9hIaj4kbBQ==/6649846324993045.jpg',
            theme: '#46718b'
        }
    ]
});
