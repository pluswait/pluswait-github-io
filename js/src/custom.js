/* 轮播背景图片 */
$(function () {
    $.backstretch([
          "source/images/9428ad7c23aec62aeab90d22af0a25e9.jpg",
          "source/images/77938890a9aaee0b3c669e8a16631398.jpg",
          "source/images/3d1a61d4d742139836ff8955cc76b399.jpg"
    ], { duration: 60000, fade: 1500 });
});
